package com.db.microservices;

import java.util.List;

public interface IAirportService {

	List<Airport> findAirportByCode(String code);

	List<Airport> findAirportByName(String name);

	List<Airport> findAirportByLatitude(String latitude);

}