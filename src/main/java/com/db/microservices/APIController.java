package com.db.microservices;

import java.util.ArrayList;
import java.util.List;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * this makes this class registered to the spring boot framework as a service
 * for Rest
 * 
 * @author niles
 *
 */
@RestController
public class APIController {

	List<User> users = new ArrayList<>();

	@GetMapping(path = "version", produces = "text/plain")
	public String getVersion() {
		return "1.0";
	}

	@GetMapping(path = "user", produces = "application/json")
	public User user() {
		User user = new User();
		user.setUsername("nilesh");
		user.setToken("xxxxxxx9293u293nskjhjkhadsf92323");
		return user;
	}

	@PostMapping(path = "adduser", consumes = "application/json", produces = "application/json")
	public @ResponseBody UserAddResponse addUser(@RequestBody User user) {
		this.users.add(user);
		System.out.println("Adding uSer...." + user.getUsername());
		UserAddResponse response = new UserAddResponse();
		response.setStatus("SUCCESS");
		response.setUser(user);
		return response;
	}

	@GetMapping(path = "allusers", produces = "application/json")
	public List<User> allUser() {
		return users;
	}
}
