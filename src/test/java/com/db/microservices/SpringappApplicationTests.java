package com.db.microservices;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.configureFor;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import com.github.tomakehurst.wiremock.WireMockServer;

@SpringBootTest
class SpringappApplicationTests {

	@Test
	void contextLoads() {
	}

	// mock for the a port 8090.... means im saying start a mock server on 8090
	private WireMockServer wireMockServer = new WireMockServer(8090);
	// client the http client which i will wrap around
	private CloseableHttpClient httpClient = HttpClients.createDefault();

	@BeforeEach
	public void init() {
		wireMockServer.start();
		configureFor("www.ourairports1.org", 8090);
	}

	@AfterEach
	public void destroy() {
		wireMockServer.stop();

	}

	@Test
	public void givenProgrammaticallyManagedServer_whenUsingSimpleStubbing_thenCorrect() throws IOException {
		// start the server
		stubFor(get(urlEqualTo("/api/test")).willReturn(aResponse().withBody("Welcome to Wiremock Testing!")));
		/**
		 * This is where iam saying that go and create new http request
		 */
		HttpGet request = new HttpGet("http://www.ourairports1.org:8090/api/test");
		HttpResponse httpResponse = httpClient.execute(request);
		
		String stringResponse = convertResponseToString(httpResponse);
		verify(getRequestedFor(urlEqualTo("/api/test")));
		assertEquals("Welcome to Wiremock Testing!", stringResponse);
	}

	private static String convertResponseToString(HttpResponse response) throws IOException {
		InputStream responseStream = response.getEntity().getContent();
		Scanner scanner = new Scanner(responseStream, "UTF-8");
		String stringResponse = scanner.useDelimiter("\\Z").next();
		scanner.close();
		return stringResponse;
	}

}
